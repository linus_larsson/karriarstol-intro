export interface User{
  fName?: string;
  lName?: string;
  age?: number;
  username?: string;
  mail?: string;
  isRegistered?: boolean;
}
