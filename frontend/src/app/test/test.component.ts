import {Component, Input, OnInit} from '@angular/core';
import { User } from '../../assets/models/user';
import {ApiService} from '../services/api.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  public users: User[] = [];

  @Input()
  test: string | undefined;

  constructor(public apiService: ApiService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  registerUser(user: User): void{
    user.isRegistered = true;
  }

  getUsers(): void{
    this.apiService.getUsers().subscribe((response: User[]) => {
      this.users = response;
    });
  }

  updateUsers(): void{
    this.apiService.updateUsers(this.users).subscribe((response: User[]) => {
      console.log(response);
    });
  }

}
