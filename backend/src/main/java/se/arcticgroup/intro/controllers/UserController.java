package se.arcticgroup.intro.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.arcticgroup.intro.models.User;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class UserController {


    User linus = new User("Linus", "Larsson", 22, "LinLar", "Linus.larsson@arcitcgroup.se", false);
    User peter = new User("Peter", "Nilsson", 42, "PetNil", "Peter.nilsson@arcitcgroup.se", false);

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers(){
        List<User> users = new ArrayList<>();

        try{
            users.add(linus);
            users.add(peter);

        }catch (Exception e){
            return ResponseEntity.status(500).build();
        }

        return ResponseEntity.ok(users);
    }

    @PutMapping("/users")
    public ResponseEntity updateUser(@RequestBody List<User> users){
        return ResponseEntity.ok(users);
    }

}
