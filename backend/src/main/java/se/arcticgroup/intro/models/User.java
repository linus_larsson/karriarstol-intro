package se.arcticgroup.intro.models;

public class User {
    public String fName;
    public String lName;
    public int age;
    public String username;
    public String mail;
    public Boolean isRegistered;

    public User(String fName, String lName, int age, String username, String mail, boolean isRegistered) {
        this.fName = fName;
        this.lName = lName;
        this.age = age;
        this.username = username;
        this.mail = mail;
        this.isRegistered = isRegistered;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(Boolean isRegistered) {
        isRegistered = isRegistered;
    }
}
